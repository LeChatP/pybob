from abc import ABC, abstractmethod
from telnetlib import Telnet
import ipaddress
from bobutils import Utils


class Configurator(ABC):
    """Base class for chain of responsability
    """

    @abstractmethod
    def set_next(self, configurator):
        """Abstrict function for next function
        """

    @abstractmethod
    def configure(self, teln: Telnet):
        """Abstract function for configuring function
        """

    def write(self, teln: Telnet, string: str):
        teln.write(string.encode("ascii"))


class AbstractConfigurator(Configurator):
    """
    The default chaining behavior can be implemented inside a base handler
    class.
    """

    _next_configurator: Configurator = None

    def set_next(self, configurator: Configurator) -> Configurator:
        """Chain of responsability set next
        """
        self._next_configurator = configurator
        return configurator

    @abstractmethod
    def configure(self, teln: Telnet):
        """Action of chain
        """
        if self._next_configurator:
            return self._next_configurator.configure(teln)
        return None


class ConfiguratorIP(AbstractConfigurator):
    """
    This class will configure network of VM
    """

    def __init__(self, gateway: str, machine):
        self._gateway, self._cidr = gateway.split('/')
        self._machine = str(ipaddress.ip_address(self._gateway) + int(machine))

    def configure(self, teln: Telnet):
        """
        This function is executing ip commands to the server
        """
        self.write(teln, "sudo ip a add " + self._machine +
                   "/" + self._cidr + " brd + dev enp0s6\n")
        Utils.prompt(teln)
        self.write(teln, "sudo ip r add default via " +
                   self._gateway + "\n")
        Utils.prompt(teln)
        super().configure(teln)


class GitCloner(AbstractConfigurator):
    """This class configure VM to clone this current project to VM
    """

    def __init__(self):
        self._url = "https://gitlab.com/LeChatP/PyBob"

    def configure(self, teln: Telnet):
        """Install git and clone project
        """
        self.write(teln, "sudo apt install -y git\n")
        Utils.prompt(teln)
        self.write(teln, "git clone {}\n".format(self._url))
        Utils.prompt(teln)
        super().configure(teln)


class ConfiguratorPython(AbstractConfigurator):
    """This class configure VM to install python
    """

    def configure(self, teln: Telnet):
        """Install git and clone project
        """
        self.write(teln, "sudo apt install -y python3 python3-pip python3-pexpect\n")
        Utils.prompt(teln)
        super().configure(teln)


class ConfiguratorZoneLDAP(AbstractConfigurator):
    """This class configure PyBob in the VM which sets the zone
    """

    def __init__(self, organization):
        self.__zone = organization

    def configure(self, teln: Telnet):
        """Set Zone on LDAP part of this project on the VM
        """
        print("set zone : '{}'".format(self.__zone))
        self.write(teln, 'echo "{}" > /home/etu/PyBob/ldap-server/zone.txt\n'.format(self.__zone))
