"""global Vars for confiuration
"""
from pathlib import Path
import json
import re

HOME = str(Path.home())
VMDIR = "{}/vm".format(HOME)
PROMPT = b"etu@vm0:~"
SCRIPTSYM = "{}/scripts".format(HOME)
PORTBASE = 2300


class Config:
    FILE = "config.json"
    __CONFIG = None

    @classmethod
    def get_config(cls):
        if cls.__CONFIG is None:
            cls.__CONFIG = Config()
        return cls.__CONFIG

    def __init__(self):
        with open(Config.FILE, "r") as jsonfile:
            self.__conf = json.load(jsonfile)

    @property
    def planet(self):
        """Planet getter
        """
        return self.__conf["planet"]

    @planet.setter
    def planet(self, planet):
        """Planet getter
        """
        self.__conf["planet"] = planet

    @property
    def name(self):
        """VM_name getter
        """
        return self.__conf["vm_name"]

    @name.setter
    def name(self, name):
        """VM_name setter
        """
        self.__conf["vm_name"] = name

    @property
    def gateway(self):
        """Gateway getter
        """
        return self.__conf["gateway"]

    @gateway.setter
    def gateway(self, gateway):
        """Gateway setter
        """
        self.__conf["gateway"] = gateway

    @property
    def resolver(self):
        """Resolver file getter
        """
        return self.__conf['file_resolver']

    @property
    def host_type(self):
        """Host type getter
        """
        return 2 if re.search(r'client', self.__conf["host_type"], re.IGNORECASE) else 1

    @host_type.setter
    def host_type(self, host_type):
        """Host type setter
        """
        self.__conf["host_type"] = 'client' if host_type > 1 else 'server'

    @property
    def tap(self):
        """TAP getter
        """
        return self.__conf["tap"]

    @tap.setter
    def tap(self, tap: int):
        """TAP setter
        """
        self.__conf["tap"] = tap

    def save(self,asker):
        self.gateway = asker.gateway
        self.host_type = asker.host_number
        self.name = asker.vm_name
        self.planet = asker.planet
        self.tap = asker.tap
        with open(Config.FILE, "w") as jsonfile:
            jsonfile.write(json.dumps(self.__conf, indent=4))
            jsonfile.close()
        print("Configuration successfully saved")
