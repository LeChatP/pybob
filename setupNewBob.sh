#!/bin/bash
cd ~
apt-get download python3-distutils
dpkg-deb -x python3-distutils_*.deb .
mkdir -p $HOME/.local
mv usr/* $HOME/.local
rmdir usr/
wget https://bootstrap.pypa.io/get-pip.py
export PYTHONPATH=$PYTHONPATH:$HOME/.local/lib/python3.8
touch $HOME/.local/lib/python3.8/distutils/__init__.py
python3.8 get-pip.py
pip3 install lxml --user
echo "You have installed locally pip3"
echo "You can execute __main__.py now"