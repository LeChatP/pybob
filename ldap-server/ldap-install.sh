export DEBIAN_FRONTEND=noninteractive
CONF=slapd-debconf.conf
cat $CONF | debconf-set-selections
apt install ldap-utils slapd -y
service slapd start