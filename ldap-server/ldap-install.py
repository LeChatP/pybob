import argparse

import pexpect
import os
import sys
import re

DPKG_ENDLINE = r"└*─+┘+"
ZONE_FILE = "zone.txt"
PASSWORD = "-etu-"
LOGFILE = "ldap-server.log"
LDIF_OU = "ou.ldif"
LDIF_USERS = "users.ldif"


class Args:
    """
    This class parse arguments
    """

    def __init__(self):
        self.__parser = argparse.ArgumentParser()
        self.__parser.add_argument('-s',
                                   dest='silent',
                                   action='store_true',
                                   help="do the work silently with config parameters")
        self.__args = self.__parser.parse_args()

    def get_args(self):
        """Return parsed args
        """
        return self.__args

    def get_parser(self):
        """Return parser args
        """
        return self.__parser


def expect_sudo_or(proc, matchs: list):
    matching = [r"\[sudo\]"]
    matching.extend(matchs)
    expect = proc.expect(matching)
    if expect == 0:
        proc.sendline("-etu-")
        if matchs:
            proc.expect(matchs)


def get_zone():
    with open(ZONE_FILE, "r") as txt:
        vzone = txt.readline()
        txt.close()
    return vzone


if os.stat(ZONE_FILE).st_size == 0:
    print("zone musn't be empty! please fill with your zone ('zone10.stri' by example)")

log = open(LOGFILE,"a")

args_obj = Args()

print("remove previous config of LDAP")
spawn = pexpect.spawn("sudo", "rm -rf /etc/ldap/slapd.d".split(' '), logfile=log, encoding='utf-8')
expect_sudo_or(spawn, [])
spawn.wait()
zone = get_zone()
dc = zone.split(".")[0]
print("Configuring setup...")
spawn = pexpect.spawn("sudo", "sed -ie 's/lab/{}/g' {} {}".format(dc,LDIF_OU,LDIF_USERS))
expect_sudo_or(spawn,[])
spawn.wait()
print("Installing LDAP...")
spawn = pexpect.spawn("sudo", "./ldap-install.sh".split(' '), logfile=log, encoding='utf-8')
expect_sudo_or(spawn, [])
spawn.wait()
print("LDAP server is configured and installed")
if not args_obj.get_args().silent and input("Next will populate database, Continue? [Y/n] ") in ["n", "N"]:
    log.close()
    sys.exit(0)
print("adding people ang groups")
spawn = pexpect.spawn("sudo", "ldapadd -cxWD cn=admin,dc={},dc=stri -f ou.ldif"
                      .format(dc).split(" "), logfile=log, encoding='utf-8')
expect_sudo_or(spawn,[r'Password:'])
spawn.sendline(PASSWORD)
if spawn.wait() != 0:
    print("Unable to add orgUnit see logFile, GLHF my friend")
    log.close()
    sys.exit(-1)
print("adding users")
spawn = pexpect.spawn("sudo", "ldapadd -cxWD cn=admin,dc={},dc=stri -f users.ldif"
                      .format(dc).split(" "), logfile=log, encoding='utf-8')
spawn.expect([r'Password:'])
spawn.sendline(PASSWORD)
if spawn.wait() != 0:
    print("Unable to add users see logFile, GLHF my friend")
    log.close()
    sys.exit(-1)
print("LDAP database is successfully populated")
print("server-side done!")
log.close()
