"""
This program permits to automates the configuration VM
from filesystem to network, next to that, the machine is setted up to work
"""
import argparse
import os
import shutil
import socket
from telnetlib import Telnet

import requests
from lxml import html

from bobutils import Utils
from configurator import (Configurator, ConfiguratorIP, ConfiguratorPython,
                          ConfiguratorZoneLDAP, GitCloner)
from configvars import HOME, PORTBASE, SCRIPTSYM, VMDIR, Config


class Args:
    """
    This class parse arguments
    """

    def __init__(self):
        self.__parser = argparse.ArgumentParser()
        self.__parser.add_argument('-s',
                                   dest='silent',
                                   action='store_true',
                                   help="do the work silently with config parameters")
        self.__args = self.__parser.parse_args()

    def get_args(self):
        """Return parsed args
        """
        return self.__args

    def get_parser(self):
        """Return parser args
        """
        return self.__parser


class NewBob:
    """
    Class handle filesystem
    """
    MASTERS_SYM = HOME + "/masters"
    ORIGIN_MASTERS_DIR = '/var/cache/kvm/masters'

    def setup_paths(self):
        """
        This create symlinks and directories where vm will be located at
        """
        if not os.path.exists(VMDIR):
            os.makedirs(VMDIR)
        if not os.path.exists(self.MASTERS_SYM):
            os.symlink(self.ORIGIN_MASTERS_DIR, self.MASTERS_SYM)
        if not os.path.exists(SCRIPTSYM):
            os.symlink(self.ORIGIN_MASTERS_DIR + "/scripts", SCRIPTSYM)

    def setup_vm(self, name):
        """ This copy image from repo to working directory
        """
        self.setup_paths()
        shutil.copyfile('/var/cache/kvm/masters/vm0-debian-testing-amd64-base.qcow2',
                        '{}/{}.qcow2'.format(VMDIR, name))


class VirtualMachine:
    """This class allow the creation of VM
    """
    SCRIPT_OVS_STARTUP = SCRIPTSYM + "/ovs-startup.sh"

    def __init__(self, name, vlan, tap, disk=0):
        self._name = name
        self._vlan = vlan
        self._ram = 1024
        self._tap = tap
        self._disk = disk
        self._connection = SimpleConnection(tap)

    @property
    def ram(self):
        """Deny Getter
        """
        return None

    @ram.setter
    def ram(self, ram: int):
        """Set RAM for VM
        """
        self._ram = ram

    def set_configuration(self, config: Configurator):
        """This sets variable to configure network
        """
        self._connection.configure(config)

    def run(self):
        """This execute the process of creation of vm with his virtual linking to tap
        """
        Utils.set_ovs_vlan(self._tap, self._vlan)
        options = ''
        if self._disk > 0 and not os.path.exists("{}/{}_disk.qcow2".format(VMDIR, self._name)):
            os.system(
                "qemu-img create -f qcow2 {}/{}_disk.qcow2 {}G"
                    .format(VMDIR, self._name, self._disk))
            options = '-drive if=none,id=newdisk,format=qcow2,media=disk,file={}/{}_disk.qcow2' \
                      ' -device virtio-blk,drive=newdisk'.format(VMDIR, self._name)
        os.system('{} {}/{}.qcow2 {} {}{}'.format(self.SCRIPT_OVS_STARTUP,
                                                  VMDIR, self._name, self._ram, self._tap, options))
        print("wait for VM running...")
        self._connection.run()


class SimpleConnection:
    """
    This simply create connection, setup everything and gives command that the user need to executes
    """

    def __init__(self, tap):
        self.__tap = tap
        self._configurator = None

    def configure(self, config: Configurator):
        """This set the network config that need to be setted
        """
        self._configurator = config

    def run(self):
        """this connect to VM and configure it if needed
        """
        teln = Telnet("localhost", PORTBASE + self.__tap)
        teln = Utils.telnet_login(teln)
        if self._configurator is not None:
            print("Configuring...")
            self._configurator.configure(teln)
            print("Connection is ready, you can focus on your work")
        teln.close()
        print("You just have to run the command :\ntelnet localhost {}".format(
            PORTBASE + self.__tap))


class ResolvParser:
    """
    This class permits to dump all TP to dictionnary of OVS
    """
    NAME_ELEMENT = 0
    IP_ELEMENT = 1
    ORGANIZATION_ELEMENT = 2

    def __init__(self, file):
        self._file = file
        self._dict = {}

    def fetch(self):
        """
        this executes the search into the resolv gateway file
        """
        self.__get_dict()

    def get_dict(self):
        """
        This return dict from fetch of file
        """
        return self._dict

    def get_iplist(self, planet):
        """This returns the list of ip fetched from planet
        """
        return self._dict[planet]

    def __get_dict(self):
        with open(self._file, 'r') as topo:
            for line in topo.readlines():
                self.__readline(line)

    def __readline(self, line):
        type_element = 0
        name = ""
        ipaddr = ""
        for element in line.strip("\n").split(' '):
            if element == "o:":
                continue
            if type_element == self.NAME_ELEMENT:
                name = self.__set_name(element)
            elif type_element == self.IP_ELEMENT:
                ipaddr = self.__set_ip(name, element)
            elif type_element == self.ORGANIZATION_ELEMENT:
                self.__set_organization(name, ipaddr, element)
            else:
                continue
            type_element += 1

    def __set_name(self, element):
        if element not in self._dict:
            self._dict[element] = {}
        return element

    def __set_ip(self, name, element):
        if element not in self._dict[name]:
            self._dict[name][element] = None
        return element

    def __set_organization(self, name, ipaddr, element):
        self._dict[name][ipaddr] = element
        return element


class VLANFinder:
    """
    This class is retrieving directly from inetdoc website
    The VLAN to use with IPCL given for the TP
    """

    def __init__(self):
        self._dict = {}

    def fetch(self):
        """
        This search in website inetdoc.net in the table.
        Every ip address with his relation with VLAN
        """
        page = requests.get(
            'https://inetdoc.net/travaux_pratiques/infra.tp/infra.tp.vlan2IPaddressing.html')
        tree = html.fromstring(page.content)
        lines = tree.xpath("//div[@class='table-contents']/table/tbody/tr")
        for line in lines:
            vlan = line.xpath('(td[not(@rowspan)])[1]/text()')[0]
            ips = line.xpath('string((td[not(@rowspan)])[2])').split('\n')
            self._dict[vlan] = ips

    def find_vlan(self, address):
        """ This returns the number of VLAN from the resulted fetch
        """
        for vlan, ips in self._dict.items():
            for ipaddr in ips:
                if ipaddr == address:
                    return vlan
        return None

    def get_dict(self):
        """This returns the full dict
        """
        return self._dict


class Asker:
    """This class asks and check inputs
    """

    def __init__(self):
        self.__tap = None
        self.__gateway = None
        self.__vm_name = None
        self.__host_number = None
        self.__disk = None
        self.__vlan = None
        self.__zone = None
        self.__planet = None
        self.__silent = False

    def ask(self, silent: bool):
        """ This is the process where everything will be asked
        """
        self.__silent = silent
        self.__tap = self.get_tap()
        if self.check_already_open():
            return False
        self.__planet = self.get_planet()
        self.__gateway = self.get_gateway(self.__planet)
        self.__vm_name = self.get_vm_name()
        self.__host_number = self.ask_host_number()
        self.__disk = self.ask_disk()
        self.__vlan = self.find_vlan()
        return True

    def check_already_open(self):
        """This will check that the current port is not already opened by the current user ONLY
        """
        a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        location = ("127.0.0.1", PORTBASE + self.__tap)
        result_of_check = a_socket.connect_ex(location)
        if result_of_check == 0:
            if not self.__silent:
                print('The server is already open on tap {}'.format(self.__tap))
                if input("Would you like to resume it ? [y/N] ") in ["y", "Y"]:
                    return True
            os.system("kill $(lsof -i | grep -m 1 2334 | cut -d ' ' -f 2)")
            print("Killed process")
        return False

    def find_vlan(self):
        """This will find the vlan from gateway
        """
        finder = VLANFinder()
        finder.fetch()
        return finder.find_vlan(self.gateway)

    @property
    def vlan(self):
        """Getter for vlan
        """
        return self.__vlan

    @property
    def planet(self):
        """Getter for planet
        """
        return self.__planet

    @property
    def tap(self):
        """Getter for tap
        """
        return self.__tap

    @property
    def gateway(self):
        """Getter for gateway
        """
        return self.__gateway

    @property
    def vm_name(self):
        """Getter for vm_name
        """
        return self.__vm_name

    @property
    def host_number(self):
        """Getter for host_number
        """
        return self.__host_number

    @property
    def disk(self):
        """Getter for disk
        """
        return self.__disk

    @property
    def zone(self):
        """Getter for ldap zone
        """
        return self.__zone

    def get_tap(self):
        """This will ask for tap to choose
        """
        tap = False
        if not self.__silent:
            tap = input(
                "Which tap will be used ? (digits only, default is {}) ".format(Config.get_config().tap))
        if not tap:
            tap = Config.get_config().tap
        else:
            assert tap.isdigit()
        return int(tap)

    def get_planet(self):
        """This will ask the planet
        """
        planet = False
        if not self.__silent:
            planet = input("What is the planet ? (default is {}) ".format(
                Config.get_config().planet)).lower()
        return planet if planet else Config.get_config().planet

    def get_gateway(self, planet):
        """This Will ask the gateway
        """
        parser = ResolvParser(Config.get_config().resolver)
        parser.fetch()
        iplist = parser.get_iplist(planet)
        choose = False
        if not self.__silent:
            print("======================")
            _ = [print('[{}] {}'.format(idx, ipaddr))
                 for idx, ipaddr in enumerate(iplist)]
            choose = input(
                "Choose ip from list that match with the TP (enter the index, default is {}) : "
                    .format(Config.get_config().gateway))
        if not choose:
            self.__zone = iplist[Config.get_config().gateway]
            return Config.get_config().gateway
        assert choose.isdigit() and len(iplist) > int(choose) >= 0
        self.__zone = iplist[list(iplist.keys())[int(choose)]]
        return list(iplist.keys())[int(choose)]

    def get_vm_name(self):
        """This will ask the name of vm (filename)
        """
        name = False
        if not self.__silent:
            name = input(
                "What is the name of VM ? (default is {}) ".format(Config.get_config().name))
            assert name.isascii()
        return name if name else Config.get_config().name

    def ask_disk(self):
        """this will ask if new storage unit is needed (32Gb by default)
        """
        if not self.__silent:
            disk = input("Did this VM needs second storage unit ? [y/N] ")
            if disk in ['y', 'Y']:
                return 32
        return 0

    def ask_host_number(self):
        """This will ask if you play the role of server or client
        """
        types = ["Server", "Client"]
        host = False
        if not self.__silent:
            _ = [print('[{}] {}'.format(idx + 1, ipaddr))
                 for idx, ipaddr in enumerate(types)]
            host = input("Select who are you (enter the index, default is {}) ".format(
                types[Config.get_config().host_type - 1]))
        if not host:
            host = Config.get_config().host_type
        else:
            assert host.isdigit() and int(host) > 0
        return int(host)


class Main:
    """This is the main class that run the program
    """

    def __init__(self):
        self.__asker = Asker()
        self._args = Args()

    def run(self):
        """This will ask and do actions in consequences
        """
        asked = self.__asker.ask(self._args.get_args().silent)
        Config.get_config().save(self.__asker)
        if not asked:
            Utils.set_ovs_vlan(self.__asker.tap, self.__asker.vlan)
            telnet = SimpleConnection(self.__asker.tap)
            telnet.run()
        else:
            self.run_vm()

    def run_vm(self):
        """This Will set up and run VM
        """
        new_bob = NewBob()
        new_bob.setup_vm(self.__asker.vm_name)
        fullconfig = ConfiguratorIP(
            self.__asker.gateway, self.__asker.host_number)
        gitconfig = GitCloner()
        zoneconfig = ConfiguratorZoneLDAP(self.__asker.zone)
        pythonconfig = ConfiguratorPython()
        virtm = VirtualMachine(self.__asker.vm_name, self.__asker.vlan,
                               self.__asker.tap, disk=self.__asker.disk)
        fullconfig.set_next(gitconfig).set_next(pythonconfig).set_next(zoneconfig)
        virtm.set_configuration(fullconfig)
        virtm.run()

if __name__ == "__main__":
    Main().run()
