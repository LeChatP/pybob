"""
Utils module
"""
import os
from configvars import PROMPT


class Utils:
    """
    This class contains functions shared
    """

    @staticmethod
    def prompt(teln):
        """
        This check if sudo ask for password to execute a command
        """
        expected = teln.expect([r"\[sudo\]".encode("ascii"), PROMPT])[0]
        if expected == 0:
            teln.write(b'-etu-\n')
            teln.read_until(PROMPT)

    @staticmethod
    def telnet_login(teln):
        """Do login for telnet
        """
        teln.write(b"\n")
        if teln.expect([PROMPT, b"login:"])[0]:
            print("Try to login...")
            teln.write(b"etu\n")
            teln.read_until(b"Password: ")
            teln.write(b"-etu-\n")
            Utils.prompt(teln)
        return teln

    @classmethod
    def set_ovs_vlan(cls, tap, vlan):
        os.system(
            'sudo ovs-vsctl set port tap{} tag={}'.format(tap, vlan))
        pass
